<?php

use yii\db\Migration;

class m180619_101648_user extends Migration
{
    /**
     CREATE TABLE `user` (
    `id` INT NOT NULL AUTO_INCREMENT,
    `username` VARCHAR(255) NOT NULL,
    `name` VARCHAR(255) NOT NULL,
    `surname` VARCHAR(255) NULL,
    `password_hash` VARCHAR(255) NOT NULL,
    `access_token` VARCHAR(255) NULL DEFAULT NULL,
    `auth_key` VARCHAR(255) NULL DEFAULT NULL,
    `created_at` INT NULL,
    `updated_at` INT NULL,
    PRIMARY KEY (`id`)
    );
     * @return bool|void
     */
    public function up()
    {
        $this->createTable('user', [
            'id' => $this->primaryKey(),
            'username' => $this->string()->notNull(),
            'name' => $this->string()->notNull(),
            'surname' => $this->string(),
            'password_hash' => $this->string()->notNull(),
            'access_token' => $this->string(),
            'auth_key' => $this->string(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ]);
    }

    public function down()
    {
        $this->dropTable('user');
    }
}
