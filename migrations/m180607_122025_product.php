<?php

use yii\db\Migration;

class m180607_122025_product extends Migration
{
    public function safeUp()
    {
        $this->createTable('product', [
            'id' => $this->primaryKey(),
            'name' => $this->string(50),
            'price' => $this->string(50),
            'created_at' => $this->integer(),
        ]);
    }

    public function safeDown()
    {
        $this->dropTable('product');
    }
}
