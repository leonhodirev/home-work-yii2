<?php

use yii\db\Migration;

class m180619_101710_access extends Migration
{
    /**
    CREATE TABLE `access` (
    `id` INT NOT NULL AUTO_INCREMENT,
    `event_id` INT NOT NULL,
    `user_id` INT NOT NULL,
    PRIMARY KEY (`id`)
    )
     * @return bool|void
     */
    public function up()
    {
        $this->createTable('access', [
            'id' => $this->primaryKey(),
            'event_id' => $this->integer()->notNull(),
            'user_id' => $this->integer()->notNull(),
        ]);
    }

    public function down()
    {
        $this->dropTable('access');
    }
}
