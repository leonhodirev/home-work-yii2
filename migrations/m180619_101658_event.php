<?php

use yii\db\Migration;

class m180619_101658_event extends Migration
{
    /**
     CREATE TABLE `event` (
    `id` INT NOT NULL AUTO_INCREMENT,
    `text` TEXT NOT NULL,
    `dt` DATETIME NOT NULL,
    `creator_id` INT NOT NULL,
    `created_at` INT NULL,
    PRIMARY KEY (`id`)
    );
     * @return bool|void
     */
    public function up()
    {
        $this->createTable('event', [
            'id' => $this->primaryKey(),
            'text' => $this->text()->notNull(),
            'dt' => $this->dateTime()->notNull(),
            'creator_id' => $this->integer(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ]);
    }

    public function down()
    {
        $this->dropTable('event');
    }
}
