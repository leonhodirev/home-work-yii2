<?php

namespace app\components;

use yii\base\Component;

class TestService extends Component
{
    public $test = '123';

    public function getViewTest()
    {
        return $this->test;
    }
}