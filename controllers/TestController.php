<?php

namespace app\controllers;

use Yii;
use app\models\Product;
use yii\db\Query;

class TestController extends \yii\web\Controller
{
    public function actionTest()
    {
//        Yii::$app->db->createCommand()->addForeignKey('fx_access_user', 'access', ['user_id'], 'user', ['id'])->execute();
//        Yii::$app->db->createCommand()->addForeignKey('fx_access_event', 'access', ['event_id'], 'event', ['id'])->execute();
//        Yii::$app->db->createCommand()->addForeignKey('fx_event_user', 'event', ['creator_id'], 'user', ['id'])->execute();
    }

    public function actionIndex()
    {

        Yii::$app->test->test = 'Привет';

//        $command = Yii::$app->db->createCommand();
//        $command->insert('user', [
//            'username' => 'leon',
//            'name' => 'leonid',
//            'surname' => 'hodirev',
//            'password_hash' => '123',
//        ]);
//            $command->insert('user', [
//                'username' => 'test',
//                'name' => 'test',
//                'surname' => 'test',
//                'password_hash' => '123',
//            ]);
//            $command->insert('user', [
//                'username' => 'abv',
//                'name' => 'abv',
//                'password_hash' => '123',
//            ]);
//        $command->execute();

        /* 4) Используя \yii\db\Query в экшене select TestController выбрать из user: */
        $query = new Query;

        // а) Запись с id=1
//        $query->select('*')
//            ->from('user')
//            ->where('id=1');

        // б) Все записи с id>1 отсортированные по имени (orderBy())
//        $query->select('*')
//            ->from('user')
//            ->where('id > 1')
//            ->orderBy('name');

        // в) количество записей.
        $query->select('count(*)')
            ->from('user');


        $user = $query->all();


//        return $this->renderContent('Привет!');
        return $this->render('index', ['user' => $user]);
    }

    public function actionInsert()
    {
        Yii::$app->db->createCommand()->batchInsert('event', ['text', 'dt'], [
            ['Привет', date("Y-m-d H:i:s")],
            ['Добрый день', date("Y-m-d H:i:s")],
            ['Пока', date("Y-m-d H:i:s")],
        ])->execute();
        Yii::$app->db->createCommand()->batchInsert('access', ['event_id', 'user_id'], [
            [1, 1],
            [2, 1],
            [3, 1],
        ])->execute();
    }

    public function actionSelect()
    {
        $query = new Query;
        $query->select('*')
            ->from('event')
            ->innerJoin('access', 'access.event_id = event.id')
            ->innerJoin('user', 'user.id = access.user_id');

        $event = $query->all();

        return $this->render('index', ['user' => $event]);
    }
}